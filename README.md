<div align="center">
  <img src="data/icons/hicolor/scalable/apps/io.gitlab.theevilskeleton.Upscaler.svg" width="128" height="128">

  # Upscaler

  Upscaler is a GTK4+libadwaita application that allows you to upscale and enhance a given image. It is a front-end for [Real-ESRGAN ncnn Vulkan](https://github.com/xinntao/Real-ESRGAN-ncnn-vulkan).

  See [Press](PRESS.md) for content mentioning Upscaler from various writers, content creators, etc.

  <img src="data/screenshots/2.png">
</div>

## Installation
<a href='https://flathub.org/apps/details/io.gitlab.theevilskeleton.Upscaler'><img width='240' alt='Download on Flathub' src='https://flathub.org/assets/badges/flathub-badge-en.png'/></a>

## Contributing
Issues and merge requests are more than welcome. However, please take the following into consideration:

- This project follows the [GNOME Code of Conduct](https://wiki.gnome.org/Foundation/CodeOfConduct)
- Only Flatpak is supported

## Development

### Prerequisites

1. Install [`org.flatpak.Builder`](https://github.com/flathub/org.flatpak.Builder) from Flathub
   ```bash
   flatpak install org.flatpak.Builder
   ```
2. Add the [GNOME Nightly](https://wiki.gnome.org/Apps/Nightly) repository
   ```bash
   flatpak remote-add --if-not-exists gnome-nightly https://nightly.gnome.org/gnome-nightly.flatpakrepo
   ```

### GNOME Builder
The recommended method is to use GNOME Builder:

1. Install [GNOME Builder](https://apps.gnome.org/app/org.gnome.Builder/) from Flathub
2. Open Builder and select "Clone Repository..."
3. Clone `https://gitlab.gnome.org/World/Upscaler.git` (or your fork)
4. Press "Run Project" (▶) at the top, or `Ctrl`+`Shift`+`[Spacebar]`.

### Flatpak
You can install Upscaler from the latest commit:

1. Clone the repository (or your fork)
   ```bash
   git clone https://gitlab.gnome.org/World/Upscaler.git
   ```
2. Build: Run the following in a terminal from the root of the repository.
   ```bash
   flatpak run org.flatpak.Builder --force-clean --install --user build-dir build-aux/io.gitlab.theevilskeleton.Upscaler.Devel.json
   ```
3. Run
   ```bash
   flatpak run io.gitlab.theevilskeleton.Upscaler.Devel
   ```

### Meson
You can build and install on your host system by directly using the Meson buildsystem:

1. Install `blueprint-compiler`
2. Run the following commands (with `/usr` prefix):
```
meson --prefix=/usr build
ninja -C build
sudo ninja -C build install
```

Alternatively, you can run [`install.sh`](./install.sh).

```
./install.sh
```
