# exceptions.py: Store exception definitions.
#
# Copyright (C) 2023 Upscaler Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-only


class AlgorithmFailed(Exception):
    """Raise when the algorithm has failed."""

    def __init__(self, result_code: int, output: str) -> None:
        super().__init__()
        self.result_code = result_code
        self.output = output

    def __str__(self) -> str:
        return (
            f"Algorithm failed.\nResult code: {self.result_code}\nOutput: {self.output}"
        )


class AlgorithmWarning(Exception):
    """Raise when the output could be damaged."""

    pass
