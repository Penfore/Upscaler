using Gtk 4.0;
using Adw 1;

template $UpscalerWindow: Adw.ApplicationWindow {
  default-width: 700;
  default-height: 600;
  width-request: 300;
  height-request: 300;
  title: _("Upscaler");

  Adw.ToastOverlay toast {
    Overlay {
      [overlay]
      Revealer drag_revealer {
        can-target: false;
        transition-type: crossfade;

        child: Adw.StatusPage {
          icon-name: "folder-download-symbolic";
          title: _("Drop to Open");

          styles [
            "drag-overlay-status-page",
          ]
        };
      }

      child: Overlay {
        [overlay]
        Revealer spin_revealer { // FIXME: The spinner shouldn't cover the headerbar
          can-target: false;
          transition-type: crossfade;

          child: Adw.Bin {
            child: Spinner spinner_loading {
              valign: center;
            };

            styles [
              "background-fill",
            ]
          };
        }

        child: Adw.NavigationView main_nav_view {
          Adw.NavigationPage {
            tag: "welcome-and-queue";
            title: _("Upscaler");

            child: WindowHandle {
              Adw.ToolbarView main_toolbar_view {
                [top]
                Adw.HeaderBar {
                  [start]
                  MenuButton {
                    icon-name: "open-menu-symbolic";
                    menu-model: primary_menu;
                    tooltip-text: _("Main Menu");
                    primary: true;
                  }
                }

                content: Stack stack_upscaler {
                  transition-type: crossfade;

                  StackPage {
                    name: "stack_welcome_page";

                    child: Adw.StatusPage status_welcome {
                      title: _("Upscaler");
                      description: _("Drag and drop images here");
                      hexpand: true;
                      vexpand: true;

                      child: Box {
                        orientation: vertical;
                        spacing: 12;

                        Button button_input {
                          valign: center;
                          halign: center;
                          label: _("_Open File…");
                          use-underline: true;

                          styles [
                            "suggested-action",
                            "pill",
                          ]
                        }
                      };
                    };
                  }

                  StackPage {
                    name: "stack_upscaling";

                    child: Adw.StatusPage {
                      title: _("Upscaling");
                      description: _("This could take a while");

                      Box {
                        orientation: vertical;
                        spacing: 36;

                        Adw.Clamp {
                          maximum-size: 300;
                          tightening-threshold: 150;

                          ProgressBar progressbar {
                            valign: center;
                          }
                        }

                        Button button_cancel {
                          valign: center;
                          halign: center;
                          label: _("_Cancel…");
                          use-underline: true;

                          styles [
                            "pill",
                          ]
                        }
                      }
                    };
                  }
                };
              }
            };
          }

          Adw.NavigationPage {
            title: _("Upscaler");
            tag: "upscaling-options";

            child: Adw.ToolbarView {
              [top]
              Adw.HeaderBar {}

              content: Adw.PreferencesPage {
                valign: center;

                Adw.PreferencesGroup {
                  Picture image {
                    halign: center;
                    height-request: 192;

                    accessibility {
                      label: _("Preview image");
                    }
                  }
                }

                Adw.PreferencesGroup {
                  title: _("Properties");

                  Adw.ActionRow action_image_size {
                    title: _("Image Size");

                    styles [
                      "property",
                    ]
                  }

                  Adw.ActionRow action_upscale_image_size {
                    title: _("Image Size After Upscaling");

                    styles [
                      "property",
                    ]
                  }
                }

                Adw.PreferencesGroup {
                  title: _("Options");
                  // Scaling is broken in Real-ESRGAN with values other than 4.
                  // Adw.ActionRow {
                  //   title: _("Upscale Ratio");
                  //   subtitle: _("The amount of times the resolution of the image will increase.");
                  //   SpinButton {
                  //     numeric: true;
                  //     valign: center;
                  //     adjustment:
                  //     Adjustment spin_scale {
                  //       step-increment: 1;
                  //       value: 4;
                  //       lower: 2;
                  //       upper: 4;
                  //     }
                  //     ;
                  //   }
                  // }
                  Adw.ComboRow combo_models {
                    title: _("Type of Image");

                    model: StringList string_models {};
                  }

                  Adw.ActionRow row_output {
                    title: _("Save Location");
                    activatable: true;
                    tooltip-text: _("Select Output Location");

                    [suffix]
                    Image row_output_icon {
                      icon-name: "document-open-symbolic";

                      accessibility {
                        label: _("Select Output Location");
                      }
                    }

                    accessibility {
                      labelled-by: row_output_icon;
                    }
                  }
                }

                Adw.PreferencesGroup {
                  Button button_upscale {
                    valign: center;
                    halign: center;
                    label: _("_Upscale");
                    tooltip-text: _("Missing Save Location");
                    use-underline: true;
                    sensitive: false;

                    styles [
                      "suggested-action",
                      "pill",
                    ]
                  }
                }
              };
            };
          }
        };
      };
    }
  }
}

menu primary_menu {
  section {
    item {
      label: _("_Open…");
      action: "app.open";
    }
  }

  section {
    // item {
    //   label: _("Preferences");
    //   action: "app.preferences";
    // }
    item {
      label: _("_Keyboard Shortcuts");
      action: "win.show-help-overlay";
    }

    item {
      label: _("_About Upscaler");
      action: "app.about";
    }
  }
}
