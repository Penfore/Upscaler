# controller.py: Controller for the main window
#
# Copyright (C) 2023 Upscaler Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-only

from gi.repository import Gio


class UpscalerController:
    def get_width(self) -> int:
        """
        @return: The width the user wants the window to be.
        """
        return self._settings.get_int("width")

    def get_height(self) -> int:
        """
        @return: The height the user wants the window to be.
        """
        return self._settings.get_int("height")

    def is_maximized(self) -> bool:
        """
        @return: If the user wants the window to be maximized or not.
        """
        return self._settings.get_boolean("is-maximized")

    def set_width(self, width: int) -> None:
        """
        Save the window width for later.
        @param width: The current width of the window.
        """
        self._settings.set_int("width", width)

    def set_height(self, height: int) -> None:
        """
        Save the window height for later.
        @param height: The current height of the window.
        """
        self._settings.set_int("height", height)

    def set_maximized(self, maximized: bool) -> None:
        """
        Save if the window is maximized for later.
        @param maximized: If the window is maximized or not.
        """
        self._settings.set_boolean("is-maximized", maximized)

    def __init__(self) -> None:
        """
        Controller for the main window of Upscaler.
        @note Variables below are hidden via "_" to ensure encapsulation.
        """
        self._settings = Gio.Settings(schema_id="io.gitlab.theevilskeleton.Upscaler")
