<?xml version="1.0" encoding="UTF-8"?>
<component type="desktop-application">
  <id>@app_id@</id>

  <name>Upscaler</name>
  <developer_name>The Upscaler Contributors</developer_name>
  <developer id="@unsuffixed_app_id@">
    <name>The Upscaler Contributors</name>
  </developer>
  <summary>Upscale and enhance images</summary>

  <metadata_license>CC-BY-SA-4.0</metadata_license>
  <project_license>GPL-3.0</project_license>

  <description>
    <p>
      Upscaler enhances and upscales images four times their original resolution. It has optimized modes for cartoons/anime and photos.
    </p>
  </description>

  <requires>
    <internet>offline-only</internet>
    <display_length compare="ge">360</display_length>
  </requires>

  <recommends>
    <control>pointing</control>
    <control>keyboard</control>
    <control>touch</control>
  </recommends>

  <screenshots>
    <screenshot type="default">
      <image>https://gitlab.gnome.org/World/Upscaler/-/raw/main/data/screenshots/1.png</image>
      <caption>Empty State</caption>
    </screenshot>
    <screenshot>
      <image>https://gitlab.gnome.org/World/Upscaler/-/raw/main/data/screenshots/2.png</image>
      <caption>Upscaling Options</caption>
    </screenshot>
    <screenshot>
      <image>https://gitlab.gnome.org/World/Upscaler/-/raw/main/data/screenshots/3.png</image>
      <caption>Upscaling Page</caption>
    </screenshot>
  </screenshots>

  <launchable type="desktop-id">@app_id@.desktop</launchable>

  <url type="homepage">https://tesk.page/upscaler</url>
  <url type="bugtracker">https://gitlab.gnome.org/World/Upscaler/-/issues</url>
  <url type="help">https://matrix.to/#/#Upscaler:gnome.org</url>
  <url type="vcs-browser">https://gitlab.gnome.org/World/Upscaler</url>
  <url type="contribute">https://gitlab.gnome.org/World/Upscaler#contributing</url>
  <url type="translate">https://gitlab.gnome.org/World/Upscaler/po</url>
  <url type="contact">https://matrix.to/#/#Upscaler:gnome.org</url>

  <content_rating type="oars-1.1" />

  <releases>
    <release version="1.2.2" date="2023-09-23">
      <description translate="no">
        <p>Add new screenshot</p>
      </description>
    </release>
    <release version="1.2.1" date="2023-09-23">
      <description translate="no">
        <p>Correct release notes</p>
      </description>
    </release>
    <release version="1.2.0" date="2023-09-23">
      <description translate="no">
        <ul>
          <li>New icon</li>
          <li>Add drag &amp; drop support</li>
          <li>New keyboard shortcuts</li>
          <li>Add labels for assistive technologies</li>
          <li>Improve support for high contrast</li>
          <li>Port to newer widgets</li>
          <li>Ellipsize text when file names are too long</li>
          <li>Add WebP support</li>
          <li>Transpose image (useful for photos taken by smartphones)</li>
          <li>New translations</li>
          <li>Add Vulkan checker</li>
        </ul>
      </description>
    </release>
    <release version="1.1.2" date="2022-12-10">
      <description translate="no">
        <ul>
          <li>Fix version</li>
        </ul>
      </description>
    </release>
    <release version="1.1.1" date="2022-12-09">
      <description translate="no">
        <ul>
          <li>Update appstream file</li>
        </ul>
      </description>
    </release>
    <release version="1.1.0" date="2022-12-09">
      <description translate="no">
        <ul>
          <li>Replace Upscaling dialog with page</li>
          <li>Add Open With functionality</li>
          <li>Improve appstream</li>
          <li>Check algorithm output in case of failure</li>
          <li>Add percentage</li>
          <li>Improved icon</li>
          <li>Suggest file name when selecting output location</li>
          <li>Rename "Open File" to "Open Image" for consistency</li>
        </ul>
      </description>
    </release>
    <release version="1.0.0" date="2022-11-15">
      <description translate="no">
        <p>Initial Upscaler release</p>
      </description>
    </release>
  </releases>

  <kudos>
      <!--
        GNOME Software kudos:
        https://gitlab.gnome.org/GNOME/gnome-software/blob/master/doc/kudos.md
      -->
      <kudo>ModernToolkit</kudo>
      <kudo>HiDpiIcon</kudo>
  </kudos>

  <custom>
    <value key="Purism::form_factor">mobile</value>
  </custom>
</component>
